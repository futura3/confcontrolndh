import express from 'express' ;
import bodyParser from 'body-parser';

import {graphiqlExpress , graphqlExpress} from 'apollo-server-express';
import { apolloUploadExpress } from 'apollo-upload-server';
import {makeExecutableSchema} from 'graphql-tools';
import mongoose from 'mongoose';
import graphqlHttp from 'express-graphql';
import {
  GraphQLObjectType,
  GraphQLSchema
 } from 'graphql'
import userQueries from './models/user/userQueries'
import profileQueries from './models/profile/profileQueries'
import entryQueries from './models/entries/entryQueries'
import roleQueries from './models/role/roleQueries'
import sessionQueries from './models/sessions/SessionQueries'
import WorkShopQueries from './models/Workshops/WorkShopQueries'

// Import GraphQL Mutations
import userMutations from './models/user/userMutations'
import profileMutations from './models/profile/profileMutations'
import entryMutations from './models/entries/entryMutations'
import roleMutations from './models/role/roleMutations'
import sessionMutations from './models/sessions/SessionMutations'
import fileMutations from './models/file/fileMutations'
import WorkShopMutations from './models/Workshops/WorkShopMutations'

import { execute, subscribe } from 'graphql';
import { createServer } from 'http';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import cors from 'cors';

var crypto = require('crypto');
//import GraphQl Subscriptions

//import userSubscription from '../models/user/userSubscription'
import entrySubscription from './models/entries/entrySubscription'
// Setup GraphQL RootQuery
let RootQuery = new GraphQLObjectType({
  name: 'Query',
  description: 'Realize Root Query',
  fields: () => ({
    user: userQueries.user,
    users: userQueries.users,
    guestusers:userQueries.guestusers,
    agentusers :userQueries.agentusers,
    userId: userQueries.userId,
    userByName: userQueries.userByName,
    profileId:profileQueries.profileId,
    roleByName:roleQueries.roleByName,
    entries:entryQueries.entries,
    activity : entryQueries.activity,
    activeSession :sessionQueries.activeSession ,
    closedSession:sessionQueries.closedSession,
    mainQuery : entryQueries.mainQuery ,
    getListOfWorkshops : WorkShopQueries.getListOfWorkshops
  })
})

// Setup GraphQL RootMutation
let RootMutation = new GraphQLObjectType({
  name: 'Mutation',
  description: 'Realize Root Mutations',
  fields: () => ({
    addUser: userMutations.addUser,
    addUserWithRole :userMutations.addUserWithRole,
    updateUser: userMutations.updateUser,
    updateUserStatus :userMutations.updateUserStatus,
    updateUserProfile:userMutations.updateUserProfile,
    updateUserPassword :userMutations.updateUserPassword,
    SwitchAgentRole :userMutations.SwitchAgentRole ,
    SwitchAgentWorkshop : userMutations.SwitchAgentWorkshop,
    signIn :userMutations.signIn,
    addRole:roleMutations.addRole,
    addProfile:profileMutations.addProfile,
    addNewProfileToUser : profileMutations.addNewProfileToUser,
    addUserWithProfile : userMutations.addUserWithProfile ,
    addEntry :entryMutations.addEntry,
    createSession : sessionMutations.createSession ,
    closeSession :sessionMutations.closeSession ,
    addWorkShop :WorkShopMutations.addWorkShop,
    uploadFile : fileMutations.singleUpload ,
    updateUserWithProfileData : userMutations.updateUserWithProfileData ,
    deleteUserWithProfile : userMutations.deleteUserWithProfile ,
    importUserList : userMutations.importUserList
  })
})
//Set up GraphQL RootSubscription
let RootSubscription = new GraphQLObjectType({
  name : 'Subscription' ,
  fields :{
    newEntry : entrySubscription.newEntry
  }
})
// Set up GraphQL Schema with our RootQuery and RootMutation
let schema = new GraphQLSchema({
  query: RootQuery,
  mutation: RootMutation ,
  subscription :RootSubscription
})
var multer  = require('multer')
mongoose.connect('mongodb://localhost/conference_ndh'  , { useMongoClient: true });
// Set up Express and integrate with our GraphQL Schema and configure to use graphiql
const PORT = 4000;
const server = express();
const storage = multer.diskStorage({
  destination: '../public/assets/avatars',
  filename(req, file, cb) {
    //let token = createToken({filename : file.originalname})
    var hash = crypto.createHash('md5').update(file.originalname).digest('hex');
    cb(null, `${hash}-${file.originalname}`);
  },
});

const upload = multer({ storage });
//server.use('*', cors({ origin: ['http://localhost:3000' ,'http://192.168.1.5:3000' , 'http://10.0.0.49:3000','http://10.0.0.66:3000', 'http://10.0.0.1:3000'] }));
server.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
server.use('/graphql',
function (req, res, next) {
 res.header('Access-Control-Allow-Origin', '*');
 res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
 if (req.method === 'OPTIONS') {
   res.sendStatus(200);
 } else {
   next();
 }},
 bodyParser.json(),
 //bodyParser.urlencoded({limit: '5mb', extended: true}),
  //apolloUploadExpress({uploadDir:"./uploads"}),
  graphqlExpress({
  schema
}));
server.post('/upload', upload.single('avatar'), async (req, res) => {
    try {
        //db.saveDatabase();
        res.send(req.file);
    } catch (err) {
        res.sendStatus(400);
    }
})
server.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
  //subscriptionsEndpoint: `ws:/localhost:4000/subscriptions`
}));

// We wrap the express server so that we can attach the WebSocket for subscriptions
const ws = createServer(server);

ws.listen(PORT, () => {
  console.log(`GraphQL Server is now running on http://localhost:${PORT}`);

  // Set up the WebSocket for handling GraphQL subscriptions
  new SubscriptionServer({
    execute,
    subscribe,
    schema,
    onOperation: (message, params, webSocket) =>
                     Object.assign(
                        params , {})
  }, {
    server: ws,
    path: '/subscriptions',
  });
});

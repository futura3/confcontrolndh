import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
 } from 'graphql'
import userType from '../user/userType'
import fileType from '../file/fileType'

// Define our user type, with two string fields; `id` and `name`
export default new GraphQLObjectType({
  name: 'Profile',
  description: 'Profile object',
  fields: () => ({
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    id: {
      type: GraphQLString
    },
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    forname:{
      type: GraphQLString
    },
    tel:{
      type: GraphQLString
    },
    avatar: {
      type: GraphQLString
    },
    function:{
      type: GraphQLString
    },
    user: {
      type: userType
    },
    region : {
      type : GraphQLString
    },
    gouvernorat : {
      type : GraphQLString
    },
    room : {
      type : GraphQLString
    }
  })
});

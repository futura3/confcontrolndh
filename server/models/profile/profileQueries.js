import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
  } from 'graphql';

import profileType from './profileType'
import profile from './profileSchema'

export default {
  profileId: {
    type: profileType,
    args: {
      id: {
        type: GraphQLID
      }
    },
    resolve: profile.getProfileByUserId
  }
};

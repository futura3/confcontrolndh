import mongoose from 'mongoose';
import user from '../user/userSchema';

var profileSchema = new mongoose.Schema({
  id: { type:String, required:true, unique:true, index:true, default:mongoose.Types.ObjectId },
  name: String,
  forname: String,
  tel: String,
  avatar :String,
  function :String ,
  region : String,
  gouvernorat : String,
  room : String,
  user : { type: mongoose.Schema.Types.ObjectId, ref: 'user' }
});

let profile = mongoose.model('profile', profileSchema);

module.exports = profile;

module.exports.getProfileByUserId = (root, {userid}) => {
  return new Promise((resolve, reject) => {
    profile.findOne({
        user: userid
    }).exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};


module.exports.addProfile = (root, {name , forname , tel , avatar  }) => {
  var newProfile = new profile({name:name , forname : forname , tel :tel , avatar : avatar});

  return new Promise((resolve, reject) => {
    newProfile.save((err, res) => {
      err ? reject(err): resolve(res);
    });
  });
}
module.exports.addNewProfileToUser = (root, {name , forname , tel , avatar , userid }) => {
  var newProfile = new profile({name:name , forname : forname , tel :tel , avatar : avatar});
  user.findOne({_id : userid}).exec(function (err, docs) {
    newProfile.user = docs._id;
    return new Promise((resolve, reject) => {
      newProfile.save((err, res) => {
        err ? reject(err): resolve(res);
      });
    });
  });

}

import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
  } from 'graphql';

import profileType from './profileType';
import profile from './profileSchema';
import { GraphQLUpload } from 'apollo-upload-server'

export default {
  addProfile:{
    type:profileType,
    args: {
      name:{
        name:'name',
        type:new GraphQLNonNull(GraphQLString)
      },
      forname:{
        name:'forname',
        type: new GraphQLNonNull(GraphQLString)
      },
      tel: {
        name:'tel',
        type: new GraphQLNonNull(GraphQLString)
      },
      avatar: {
        name:'avatar',
        type: new GraphQLNonNull(GraphQLString)
      } 

    },
    resolve: profile.addProfile
  } ,
  addNewProfileToUser:{
    type:profileType,
    args: {
      name:{
        name:'name',
        type:new GraphQLNonNull(GraphQLString)
      },
      forname:{
        name:'forname',
        type: new GraphQLNonNull(GraphQLString)
      },
      tel: {
        name:'tel',
        type: new GraphQLNonNull(GraphQLString)
      },
      avatar: {
        name:'avatar',
        type: new GraphQLNonNull(GraphQLString)
      },
      userid: {
        name:'userid',
        type: new GraphQLNonNull(GraphQLString)
      }
    },
    resolve: profile.addNewProfileToUser
  }
};

import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
  } from 'graphql';

import entryType , {edges , pageInfo} from './entryType'
import entry from './entrySchema'
export const PaginatedQuery = new GraphQLObjectType({
  name: 'PaginatedQuery',
  description: 'The main query',
  fields: () => ({
    totalCount: {
      type: GraphQLInt,
      description: 'Total Count of rows'
    },
    edges: {
      type: new GraphQLList(edges),
      description: 'Edges Part of the Query'
    },
    pageInfo:{
      type: pageInfo,
      description: 'Page Information Part of the Query '
    }
  })
});
export default {
  entries: {
    type: new GraphQLList(entryType),
    resolve: entry.getListOfEntries
  },
  activity :{
    type: new GraphQLList(entryType),
    resolve: entry.getEntriesListByUserId,
    args: {
      id:{
        name:'id',
        type: new GraphQLNonNull(GraphQLString)
      }
  }},
  mainQuery: {
            type: PaginatedQuery,
            args:{
              first:{
                name:"first",
                type: (GraphQLInt)
              },
              after:{
                name:"after",
                type: (GraphQLString)
              }
            },
            resolve : entry.mainpaginatedQuery
          }

}

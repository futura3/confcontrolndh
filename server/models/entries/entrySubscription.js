import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
  } from 'graphql';

import entryType from './entryType';
import entry from './entrySchema';
export default {
  newEntry :{
    type: entryType,
    resolve : entry.newEntry

  }

}

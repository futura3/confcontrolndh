import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID ,
  GraphQLBoolean
 } from 'graphql'
 import {
 GraphQLDate,
 GraphQLTime,
 GraphQLDateTime
} from 'graphql-iso-date';
import userType from '../user/userType';
import sessionType from '../sessions/sessionType';

// Define our user type, with two string fields; `id` and `name`
const entryType = new GraphQLObjectType({
  name: 'Entry',
  description: 'Entry object',
  fields: () => ({
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    entryId: {
      type: (GraphQLInt),
      description: 'Item number '
    },
    dateEntry: {
      type: new GraphQLNonNull(GraphQLDateTime)
    },
    action:{
      type: GraphQLString
    },
    user:{
      type: userType
    },
    agent: {
      type: userType
    },
    entrysession :{
      type : sessionType
    }
  })
});
export default entryType ;
export const edges = new GraphQLObjectType({
  name: 'edges',
  description: 'The edge part of the query',
  fields: () => ({
    node: {
      type: (entryType),
      description: 'Each entry item '
    },
    cursor: {
      type: GraphQLString,
      description: 'Each item cursor '
    }
  })
});
export const pageInfo = new GraphQLObjectType({
  name: 'pageInfo',
  description: 'Key information for the page',
  fields: () => ({
    endCursor: {
      type: GraphQLString,
      description: 'Last cursor '
    },
    hasNextPage:{
      type: GraphQLBoolean,
      description:"Flag to denote end of all Entry"
    }
  })
});

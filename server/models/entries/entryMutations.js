import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
  } from 'graphql';

import entryType from './entryType';
import entry from './entrySchema';

export default {
  addEntry:{
    type:entryType,
    args: {
      action:{
        name:'action',
        type: new GraphQLNonNull(GraphQLString)
      },
      agent: {
        name:'agent',
        type: new GraphQLNonNull(GraphQLString)
      },
      user: {
        name:'user',
        type: new GraphQLNonNull(GraphQLString)
      }
    },
    resolve: entry.addEntry
  }
};

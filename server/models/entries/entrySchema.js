import mongoose from 'mongoose';
import session from '../sessions/sessionSchema';
import {pubsub} from '../../pubsub'
 //const pubsub = new PubSub(); //create a PubSub instance
const ENTRY_ADDED = 'newEntry';
var autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose.connection);


var entrySchema = new mongoose.Schema({
  entryId: {
    type : Number
  },
  dateEntry: Date,
  action: {
    type :String,
    enum : ['IN','OUT'],
  },
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
  agent :{ type: mongoose.Schema.Types.ObjectId, ref: 'user' },
  workshop :{type: mongoose.Schema.Types.ObjectId, ref: 'workshop'},
  entrysession :{
    type: mongoose.Schema.Types.ObjectId, ref: 'session'
  }
});

entrySchema.plugin(autoIncrement.plugin, { model: 'entry', field: 'entryId' });
let entry = mongoose.model('entry', entrySchema);
let totalCount = 0
module.exports = entry;

module.exports.getListOfEntries = () => {
  return new Promise((resolve, reject) => {
    entry.find({}).populate({
    path: 'user agent',
    // Get friends of friends - populate the 'friends' array for every friend
    populate: { path: 'profile' }
}).exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};

module.exports.getEntryById = (root, {id}) => {
  return new Promise((resolve, reject) => {
    entry.findOne({
        id: id
    }).exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};

module.exports.getEntriesListByUserId = (root, {id}) => {
  return new Promise((resolve, reject) => {
    entry.find({
      user : id
    }).populate({
    path: 'user agent',
    // Get friends of friends - populate the 'friends' array for every friend
    populate: { path: 'profile' }
}).exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};


module.exports.addEntry = (root, { action , user , agent }) => {
  var newEntry = new entry({dateEntry:new Date() , action:action , user :user , agent :agent});
  session.findOne({
      stat: "ON"
  }).exec((err, res) => {
    console.log(res)
    if(res!=null)
    newEntry.entrysession = res._id;
    return new Promise((resolve, reject) => {
      newEntry.save((err, res) => {
        err ? reject(err): resolve(res);
      });
    });
  });

    // pubsub.publish(ENTRY_ADDED, { 'newEntry': {
    //   id : newEntry.id ,
    //   action :newEntry.action
    // }});
  // return new Promise((resolve, reject) => {
  //   newEntry.save((err, res) => {
  //     err ? reject(err): resolve(res);
  //   }).then(res=>{
  //
  //           console.log(res)
  //   });
  // });
}
module.exports.newEntry=()=> {  // create a Entrydded subscription resolver function.
  subscribe: () => pubsub.asyncIterator(ENTRY_ADDED)  // subscribe to changes in an entry
}
module.exports.mainpaginatedQuery = (root,{first,after}) => {
                let edgesArray = []
                //<<<Tip>>> - Decoding the cursor value from Base 64 to integer.
                let cursorNumeric =  parseInt(Buffer.from(after,'base64').toString('ascii'))
                if (!cursorNumeric) cursorNumeric = 0

                //<<<Tip>>> - Use Promise to handle Async behaviour of Mongoose
                var edgesAndPageInfoPromise = new Promise((resolve,reject)=>{
                   //<<<TIP>>> where and gt are used to check against Args.after(cursorNumeric)
                   let edges =  entry.where('entryId').gt(cursorNumeric).find({},(err,result)=>{
                          if (err){
                            console.error("---Error "+ err)
                          }

                       }).populate({
                         path: 'user agent',
                         // Get friends of friends - populate the 'friends' array for every friend
                         populate: { path: 'profile' }
                       }).limit(first).cursor() //<<<TIP>>> Limit is obtained from Args.first

                   edges.on('data',res => {
                     console.log(res)
                      edgesArray.push({
                          //<<<Tip>>> - Encoding the cursor value to Base 64 as suggested in GraphQL documentation.
                          cursor: Buffer.from((res.id).toString()).toString('base64'),
                          node: {
                            entryId:res.entryId,
                            dateEntry:res.dateEntry ,
                            action :res.action ,
                            agent : res.agent,
                            user : res.user
                          }
                      })
                   })

                   edges.on('error',err => {
                      reject(err)
                   })

                   edges.on('end',() => {
                      let endCursor = edgesArray.length>0?edgesArray[edgesArray.length-1].cursor:NaN

                      let hasNextPageFlag = new Promise((resolve,reject)=>{
                          if (endCursor){
                            let endCursorNumeric = parseInt(Buffer.from(endCursor,'base64').toString('ascii'))
                            entry.where('id').gt(endCursorNumeric).count((err,count)=>{
                                // console.log(":::DEBUG::: Has Next Page Count? "+ count)
                                count >0 ? resolve(true):resolve(false)
                            })
                          }
                          else resolve(false)
                      })

                      // console.info(":::info::: Cursor Ended")

                      resolve(
                        {
                          edges:edgesArray,
                          pageInfo:{
                            endCursor:endCursor,
                            hasNextPage:hasNextPageFlag
                          }
                        }
                      )
                   })
                })
                let totalCountPromise = new Promise((resolve,reject)=>{
                    if (totalCount ===0) {
                      totalCount = entry.count((err,count)=>{
                        if (err) reject(err)
                        resolve(count)
                      })
                    }
                    else resolve(totalCount)
                })

                let returnValue = Promise.all([edgesAndPageInfoPromise,totalCountPromise]).then((values) => {
                    return {
                      edges:values[0].edges,
                      totalCount:values[1],
                      pageInfo:{
                        endCursor:values[0].pageInfo.endCursor,
                        hasNextPage:values[0].pageInfo.hasNextPage
                      }
                    }
                })
                return returnValue
          }

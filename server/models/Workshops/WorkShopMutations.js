import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
  } from 'graphql';

import workType from './workshopType';
import workshop from './WorkShopSchema';

export default {
  addWorkShop:{
    type:workType,
    args: {
      name:{
        name:'name',
        type:new GraphQLNonNull(GraphQLString)
      }
    },
    resolve: workshop.addWorkShop
  }
};

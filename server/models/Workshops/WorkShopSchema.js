import mongoose from 'mongoose';

var autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose.connection);


var WorkShopSchema = new mongoose.Schema({
  workshopId: {
    type : Number
  },
  name: {
    type : String
  },
});

WorkShopSchema.plugin(autoIncrement.plugin, { model: 'workshop', field: 'workshopId' });
let workshop = mongoose.model('workshop', WorkShopSchema);

module.exports = workshop;

module.exports.getListOfWorkshops = () => {
  return new Promise((resolve, reject) => {
    workshop.find({}).exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};

module.exports.getWorkshopById = (root, {id}) => {
  return new Promise((resolve, reject) => {
    workshop.findOne({
        workshopId: id
    }).exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};




module.exports.addWorkShop = (root, {name }) => {
  var newwork = new workshop({name : name});
  return new Promise((resolve, reject) => {
    newwork.save((err, res) => {
      err ? reject(err): resolve(res);
    });
  });
}

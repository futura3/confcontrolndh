import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
  } from 'graphql';

import workType from './workshopType';
import workshop from './WorkShopSchema';

export default {
  getListOfWorkshops: {
    type: new GraphQLList(workType),
    resolve: workshop.getListOfWorkshops,
  }
};

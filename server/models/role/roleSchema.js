import mongoose from 'mongoose';

var roleSchema = new mongoose.Schema({
  id: { type:String, required:true, unique:true, index:true, default:mongoose.Types.ObjectId },
  name :String ,
  type :{
    type :String,
    enum : ['IN','OUT']
  }
});

let role = mongoose.model('role', roleSchema);

module.exports = role;


module.exports.getRoleByRoleName = (root, {name}) => {
  return new Promise((resolve, reject) => {
    role.findOne({
      name: name
    }).exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};


module.exports.addRole = (root, {name }) => {
  var newRole = new role({name:name});

  return new Promise((resolve, reject) => {
    newRole.save((err, res) => {
      err ? reject(err): resolve(res);
    });
  });
}

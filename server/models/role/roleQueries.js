import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
  } from 'graphql';

import roleType from './roleType'
import role from './roleSchema'

export default {
  roleByName: {
    type: roleType,
    args: {
      name: {
        type: GraphQLString
      }
    },
    resolve: role.getRoleByRoleName
  }
};

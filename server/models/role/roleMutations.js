import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
  } from 'graphql';

import roleType from './roleType';
import role from './roleSchema';

export default {
  addRole:{
    type:roleType,
    args: {
      name:{
        name:'name',
        type:new GraphQLNonNull(GraphQLString)
      }
    },
    resolve: role.addRole
  }
};

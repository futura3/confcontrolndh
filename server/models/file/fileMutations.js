import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
  } from 'graphql';
  import  fileType from './fileType';
  import {Upload} from './fileType';
  import file from './fileSchema';
  export default {
    singleUpload:{
      type :fileType,
      args :{
        file: {
          name:'file',
          type: Upload
        }
      },
      resolve : file.singleUpload
    }
  }

import mongoose from 'mongoose';
import user from '../user/userSchema';
import mkdirp from 'mkdirp'
import shortid from 'shortid'
import { createWriteStream } from 'fs'
const uploadDir = './../uploads'

var fileSchema = new mongoose.Schema({
  id: { type:String, required:true, unique:true, index:true, default:mongoose.Types.ObjectId },
  path :String ,
  filename :String ,
  mimetype :String ,
  encoding:String
});

let file = mongoose.model('file',fileSchema);
mkdirp.sync(uploadDir)

module.exports = file;

const storeUpload = async ({ stream, filename }) => {
  const id = shortid.generate()
  const path = `${uploadDir}/${id}-${filename}`

  return new Promise((resolve, reject) =>
    stream
      .pipe(createWriteStream(path))
      .on('finish', () => resolve({ id, path }))
      .on('error', reject)
  )
}
const processUpload = async upload => {

  const { stream, filename, mimetype, encoding } = await upload
  console.log(upload)
  const { id, path } = await storeUpload({ stream, filename });
  newFile = new file({id : id , filename :filename , encoding : encoding , path :path});
  return new Promise((resolve, reject) => {
    newFile.save((err, res) => {
      err ? reject(err): resolve(res);
    });
  });
}
module.exports.singleUpload =(obj, { file }) => {
  console.log(file)
  processUpload(file)};

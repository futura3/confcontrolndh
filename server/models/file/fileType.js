import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID,
  GraphQLScalarType
 } from 'graphql'
 export const Upload =  new GraphQLScalarType({
  name : 'Upload' ,
  serialize: value => {
      return value;
    },
    parseValue: value => {
      return value;
    },
    parseLiteral: ast => {
      return ast.value;
    }

  })
 // Define our user type, with two string fields; `id` and `name`
export default new GraphQLObjectType({
 name : 'fileType' ,
 fields:()=>({
   _id: {
     type: new GraphQLNonNull(GraphQLID)
   },
   path: {
     type: GraphQLString
   },
   filename: {
     type: GraphQLString
   },
   mimetype: {
     type: GraphQLString
   },
   encoding: {
     type: GraphQLString
   }
 })

 })

import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
  } from 'graphql';

  import sessionType from './sessionType'
  import session from './sessionSchema'

export default {
  createSession :{
    type : sessionType ,
    resolve : session.addNewSession
  },
  closeSession :{
    type : sessionType ,
    resolve : session.closeSession
  }
}

import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
  } from 'graphql';

import sessionType from './sessionType'
import session from './sessionSchema'

export default {
  activeSession :{
    type : sessionType ,
    resolve : session.getActiveSession
  },
  closedSession :{
    type : sessionType ,
    resolve : session.getclosedSession
  }

}

import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID
 } from 'graphql'
 import {
 GraphQLDate,
 GraphQLTime,
 GraphQLDateTime
} from 'graphql-iso-date';
// Define our user type, with two string fields; `id` and `name`
export default new GraphQLObjectType({
  name: 'sessionType',
  description: 'SessionEntries object',
  fields: () => ({
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    date: {
      type: new GraphQLNonNull(GraphQLDateTime)
    },
    start_hour:{
      type: new GraphQLNonNull(GraphQLDateTime)
    },
    end_hour:{
      type: new GraphQLNonNull(GraphQLDateTime)
    },
    stat: {
     type: new GraphQLNonNull(GraphQLString)
    }
  })
});

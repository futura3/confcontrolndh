import mongoose from 'mongoose';
import PubSub from '../../pubsub'
import user from '../user/userSchema';




var sessionSchema = new mongoose.Schema({
  id: { type:String, required:true, unique:true, index:true, default:mongoose.Types.ObjectId },
  date: Date,
  start_hour :Date ,
  end_hour :Date ,
  stat : {
    type :String,
    enum : ['ON','OFF'],
  }
});

let session = mongoose.model('session', sessionSchema);

module.exports = session;
module.exports.getActiveSession = (root) => {
  return new Promise((resolve, reject) => {
    session.findOne({
        stat: "ON"
    }).exec((err, res) => {
      console.log(res)
      err ? reject(err) : resolve(res);
    });
  });
};
module.exports.getclosedSession = (root) => {
  return new Promise((resolve, reject) => {
    session.findOne({
        stat: "OFF"
    }).exec((err, res) => {
      console.log(res)
      err ? reject(err) : resolve(res);
    });
  });
};
module.exports.addNewSession =(root , {})=>{
  let date = new Date();
  var newSession = new session({date:date , start_hour : date , end_hour:null , stat : "ON"});

  return new Promise((resolve, reject) => {
    newSession.save((err, res) => {
      err ? reject(err): resolve(res);
    });
  });
}
module.exports.closeSession=(root , {id})=>{
  let updatedSession ={
    end_hour : new Date() ,
    stat : "OFF"
  }
  return new Promise((resolve, reject) => {
    session.findOneAndUpdate(
        { stat: "ON" },
        { $set: updatedSession },
        { returnNewDocument: true }
    ).exec((err, res) => {
      err ? reject(err) : resolve(res);
    }).then(res=>{
      user.update(
        {"status": "IN"}, //query, you can also query for email
        {$set: {"status": "OUT"}},
        {"multi": true} //for multiple documents
        ).exec()
    });
  });
}

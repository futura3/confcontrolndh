import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID,
  GraphQLInputObjectType
  } from 'graphql';

import userType from './userType';
import user from './userSchema';
const ProfileInputType = new GraphQLInputObjectType({
  name: 'ProfileInput',
  fields: () => ({
    _id:  { type: GraphQLID },
    name:  { type: GraphQLString },
    forname: { type: GraphQLString},
    tel:        { type: GraphQLString },
    avatar:        { type: GraphQLString },
    region:        { type: GraphQLString },
    gouvernorat:        { type: GraphQLString },
    function:        { type: GraphQLString }
  })
})
const UserInputType = new GraphQLInputObjectType({
  name: 'UserInput',
  fields: () => ({
    _id:  { type: GraphQLID },
    cin:  { type: GraphQLString },
    identifiant: { type: GraphQLString},
    username :{type :GraphQLString },
    status : {type : GraphQLString},
    profile: { type: ProfileInputType}
  })
});
export default {
  addUser:{
    type:userType,
    args: {
      username:{
        name:'username',
        type:new GraphQLNonNull(GraphQLString)
      },
      password:{
        name:'password',
        type: new GraphQLNonNull(GraphQLString)
      }
    },
    resolve: user.addUser
  },
  addUserWithRole :{
    type:userType,
    args: {
      username:{
        name:'username',
        type:new GraphQLNonNull(GraphQLString)
      },
      password:{
        name:'password',
        type: new GraphQLNonNull(GraphQLString)
      },
      rolename: {
        name:'rolename',
        type: new GraphQLNonNull(GraphQLString)
      },
      identifiant: {
        name:'identifiant',
        type: new GraphQLNonNull(GraphQLString)
      }
    },
    resolve: user.addUserWithRole
  },
  updateUserProfile:{
    type:userType,
    args:{
      userid:{
        type: GraphQLID
      },
      profileId :{
        type: GraphQLID
      }
    },
    resolve : user.updateUserProfile
  },
  deleteUserWithProfile :{
    type:userType,
    args:{
      userId:{
        type: GraphQLID
      },
      profileId :{
        type: GraphQLID
      }
    },
    resolve : user.deleteUserWithProfile
  },
  updateUserWithProfileData:{
    type:userType,
    args: {
      userObj: { type: UserInputType }
    },
    resolve : user.updateUserWithProfileData
  },
  updateUser:{
    type:userType,
    args: {
      id:{
        type: GraphQLID
      },
      username:{
        name:'username',
        type:new GraphQLNonNull(GraphQLString)
      },
      status:{
        name:'status',
        type: new GraphQLNonNull(GraphQLString)
      }
    },
    resolve: user.updateUser
  },
  updateUserStatus :{
    type:userType,
    args: {
      id:{
        type: GraphQLID
      },
      status:{
        name:'status',
        type: new GraphQLNonNull(GraphQLString)
      },
      agent:{
        name:'agent',
        type: new GraphQLNonNull(GraphQLString)
      }
    },
    resolve: user.updateUserStatus
  } ,
  signIn :{
    type:  new GraphQLObjectType({
    name: 'signIn',
    fields: { user: { type: userType }, token: { type: GraphQLString } }
  }),
    args: {
      username:{
        type: new GraphQLNonNull(GraphQLString)
      },
      password:{
        name:'password',
        type: new GraphQLNonNull(GraphQLString)
      }
    },
    resolve: user.signIn
  } ,
  updateUserPassword :{
    type:userType,
    args: {
      username:{
        type: new GraphQLNonNull(GraphQLString)
      },
      password:{
        name:'password',
        type: new GraphQLNonNull(GraphQLString)
      }
    },
    resolve: user.updateUserPassword
  } ,
  SwitchAgentRole :{
    type:userType,
    args: {
      id:{
        type: GraphQLID
      },
      agentfunction:{
        name:'agentfunction',
        type: new GraphQLNonNull(GraphQLString)
      }
    },
    resolve: user.SwitchAgentRole
  },
  SwitchAgentWorkshop :{
    type:userType,
    args: {
      id:{
        type: GraphQLID
      },
      workshopId:{
        type: GraphQLID
      },
    },
    resolve: user.SwitchAgentWorkshop
  } ,
  addUserWithProfile : {
    type : userType ,
    args: {
      identifiant :{
        type : GraphQLString
      },
      name:{
        name : 'name' ,
        type: new GraphQLNonNull(GraphQLString)
      },
      forname:{
        name:'forname',
        type: new GraphQLNonNull(GraphQLString)
      },
      cin:{
        name:'cin',
        type:  GraphQLString
      },
      fonction:{
        name:'fonction',
        type: GraphQLString
      },
      tel:{
        name:'tel',
        type: GraphQLString
      },
      avatar:{
        name:'avatar',
        type: GraphQLString
      },
      region:{
        name:'region',
        type: GraphQLString
      },
      gouvernorat:{
        name :'gouvernorat' ,
        type :GraphQLString
      }
    },
    resolve: user.addUserWithProfile
  },
  importUserList :{
    type : userType ,
    resolve : user.importUserList
  }
};

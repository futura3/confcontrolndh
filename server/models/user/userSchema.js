import mongoose from 'mongoose';
import role from '../role/roleSchema';
import profile from '../profile/profileSchema';
import entry from '../entries/entrySchema';
import workshop from '../Workshops/WorkShopSchema';
import {
  createToken,
  verifyToken,
  encryptPassword,
  comparePassword
}  from '../../utils/auth';
var uniqueValidator = require('mongoose-unique-validator');
import bcrypt from 'bcrypt';
import users_list from './csvusers'
const MainWorkShopID = '5a327196e2b1af56639a13d1';
const userSchema = new mongoose.Schema({
  id: { type:String, required:true, unique:true, index:true, default:mongoose.Types.ObjectId },
  username: String,
  password: String,
  cin : {
    type:String
  },
  identifiant : {
    type :String ,
    unique:true
  } ,
  role: { type: mongoose.Schema.Types.ObjectId, ref: 'role' },
  profile :{type: mongoose.Schema.Types.ObjectId, ref: 'profile'},
  workshop :{type: mongoose.Schema.Types.ObjectId, ref: 'workshop'},
  status :{
    type : String,
    enum : ['IN','OUT' , 'ABSCENT'],
       default: 'ABSCENT'
     }
});
userSchema.plugin(uniqueValidator);
userSchema.pre('save', function (next) {
  var user = this;
  let h_p ;
  if(user.password)
    h_p = user.password;
  else h_p = "password";
  bcrypt.hash(h_p, 10, function (err, hash){
    if (err) {
      return next(err);
    }
    user.password = hash;
    next();
  })
});

let user = mongoose.model('user', userSchema);
module.exports = user;

module.exports.getListOfUsers = () => {
  return new Promise((resolve, reject) => {
    user.find({})
    .populate('profile')
    .exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};
module.exports.getListOfGuestUsers =() =>{

    return new Promise((resolve, reject) => {
      user.find({role:'5a2647aa73bdd4037d28f4c5'})
      .populate('profile')
      .exec((err, res) => {
        err ? reject(err) : resolve(res);
      });

})
}
module.exports.getListOfAgentUsers =()=>{
  return new Promise((resolve, reject) => {
    user.find({role: { $in: ['5a2e916a89b9d30ed25541d6', '5a2e917089b9d30ed25541d8' ,'5a327165e2b1af56639a13d0']}})
    .populate('profile')
    .populate('role')
    .populate('workshop')
    .exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
})
}

module.exports.getUserById = (root, {id}) => {
  return new Promise((resolve, reject) => {
    user.findOne({
        _id: id
    }).populate('profile').exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};

module.exports.getUserByUserName = (root, {username}) => {
  return new Promise((resolve, reject) => {
    user.findOne({
      username: username
    }).exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
};

module.exports.getUserByPosition = (root, {id}) => {
  return new Promise((resolve, reject) => {
    user.find({}).exec((err, res) => {
      err ? reject(err) : resolve(res[id]);
    });
  });
};

module.exports.addUser = (root, {username , password  }) => {

  var newUser = new user({username:username ,password :password});

  return new Promise((resolve, reject) => {
    newUser.save((err, res) => {
      err ? reject(err): resolve(res);
    });
  });
}
module.exports.addUserWithRole = (root, {username , password , rolename , identifiant}) => {
  var r = null;
  var newUser = new user({username:username ,password :password , identifiant : identifiant});
  role.findOne({name : rolename}).exec(function (err, docs) {
    newUser.role = docs._id;
    return new Promise((resolve, reject) => {
      newUser.save((err, res) => {
        err ? reject(err): resolve(res);
      });
    });
  });
}
module.exports.addUserWithProfile = (root, {identifiant , name , forname  , tel , fonction, avatar , cin, region , gouvernorat  }) => {
    var r = 'guest';
    var newUser = new user({identifiant:identifiant , username:cin ,password :cin , cin :cin});
    var newProfile = new profile({
      name : name ,
      forname : forname ,
      tel :tel ,
      avatar :avatar ,
      function : fonction,
      region: region ,
      gouvernorat:gouvernorat
    });
    role.findOne({name : r}).exec(function (err, docs) {
      newUser.role = docs._id;
      return new Promise((resolve, reject) => {
        newUser.save((err, res) => {
          console.log(res)
          newProfile.user = res._id ;
          newProfile.save((err, res) => {
            //err ? reject(err): resolve(res);
            user.findOneAndUpdate(
                { _id: newProfile.user},
                { $set: {profile : newProfile._id} },
                { returnNewDocument: true }
            ).exec((err, res) => {
              //console.log(res)
              err ? reject(err): resolve(res);

            })
          });
        });
      });
    });
}
module.exports.updateUser = (root, {id, username , status}) => {
  var updateUser = {username:username , status :status};
  return new Promise((resolve, reject) => {
    user.findOneAndUpdate(
        { id: id },
        { $set: updateUser },
        { returnNewDocument: true }
    ).exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
}

module.exports.updateUserProfile = (root , {userid , profileId}) => {
  profile.findOne({_id : profileId}).exec(function (err, docs) {
    var updateUser = {profile :docs._id};
    return new Promise((resolve, reject) => {
      user.findOneAndUpdate(
          { _id: userid },
          { $set: {profile : docs._id} },
          { returnNewDocument: true }
      ).exec((err, res) => {
        err ? reject(err) : resolve(res);
      });
    });
})
}
module.exports.updateUserWithProfileData = (root , {userObj}) => {
  console.log(userObj)
  let set_profile =userObj.profile;
  profile.findOneAndUpdate(
      {_id : userObj.profile._id },
      {$set : set_profile} ,
      { returnNewDocument: true }).exec(function (err, docs) {
      user.findOneAndUpdate(
          { _id: userObj._id },
          { $set: {cin : userObj.cin , identifiant : userObj.identifiant} },
          { returnNewDocument: true }
      ).exec((err, res) => {

      });
})
}
module.exports.updateUserStatus=(root,{id , status , agent})=>{
  var updateUser = {status :status};
  return new Promise((resolve, reject) => {
    user.findOneAndUpdate(
        { _id: id },
        { $set: updateUser },
        { returnNewDocument: true }
    ).exec((err, res) => {

      err ? reject(err) : resolve(res);
    });
  }).then(res=>{
    var newEntry = new entry({dateEntry:new Date() , action:status , user :id , agent :agent});

    return new Promise((resolve, reject) => {
      newEntry.save((err, res) => {
        err ? reject(err): resolve(res);
      });
    });
  });
}
module.exports.signIn = (root , {username , password})=>{
  return new Promise((resolve, reject) => {
        // Validate the data
        if (!username) {
          return reject({
            code: 'username.empty',
            message: 'Username is empty.'
          });
        }

        if (!password) {
          return reject({
            code: 'password.empty',
            message: 'You have to provide a password.'
          });
        }

        // Find the user
        return user.findOne({ username: username }).populate('role')
          .then((user) => {
            if (!user) {
              return reject({
                code: 'user.not_found',
                message: 'Authentication failed. User not found.'
              });
            }

            return comparePassword(user.password, password, (err, isMatch) => {
              if (err) { return reject(err); }
              if (!isMatch) {
                return reject({
                  code: 'password.wrong',
                  message: 'Wrong password.'
                });
              }
              return resolve({token : createToken({ id: user._id, username: user.username }) , user :user});
              //return resolve(user)
            });
          })
          .catch(err => reject(err));
      });
}
module.exports.isAuthenticated =(root , {})=>{
  return new Promise((resolve, reject) => {
        if (!args.token) {
          return reject({
            code: 'token.empty',
            message: 'The user token is empty.'
          });
        }

        return verifyToken(args.token, (err, decoded) => {
          if (err) {
            return reject({
              code: 'user.unauthenticated',
              message: 'You must be authenticated.'
            });
          }

          return resolve(decoded);
        });
      });
}
module.exports.updateUserPassword=(root , {username , password})=>{
  bcrypt.hash(password, 10, function (err, hash){
    if (err) {
      return next(err);
    }
    return new Promise((resolve, reject) => {
      user.findOneAndUpdate(
          { username: username },
          { $set: {password:hash} },
          { returnNewDocument: true }
      ).exec((err, res) => {
        err ? reject(err) : resolve(res);
      });
    });
  });

}
module.exports.SwitchAgentRole =(root , {id , agentfunction})=>{
  let role_name = null;
  if(agentfunction=="enter"){
    role_name = "agent_in";
  }
  if(agentfunction =="exit"){
    role_name ="agent_out"
  }
  role.findOne({name : role_name}).exec(function (err, docs) {
    return new Promise((resolve, reject) => {
      user.findOneAndUpdate(
        { _id: id },
        { $set: {role:docs._id} },
        { returnNewDocument: true }
    ).exec((err, res) => {
      err ? reject(err) : resolve(res);
    });
  });
  });
}
module.exports.SwitchAgentWorkshop =(root , {id , workshopId})=>{
  workshop.findOne({_id : workshopId}).exec(function (err, docs) {
    if(MainWorkShopID==workshopId){
      return new Promise((resolve, reject) => {
        user.findOneAndUpdate(
          { _id: id },
          { $set: {workshop:docs._id} },
          { returnNewDocument: true }
      ).exec((err, res) => {
        err ? reject(err) : resolve(res);
      });
    });
  }else{
    let work_id = docs._id;
    role.findOne({name : "agent_workshop"}).exec(function (err, docs) {
      return new Promise((resolve, reject) => {
        user.findOneAndUpdate(
          { _id: id },
          { $set: {workshop:work_id , role : docs._id}},
          { returnNewDocument: true }
      ).exec((err, res) => {
        err ? reject(err) : resolve(res);
      });
    });
  })
}
})
}
module.exports.deleteUserWithProfile=(root , {userId , profileId})=>{
  profile.find({ _id:profileId }).remove().exec(function(err , docs){
    user.find({ _id:userId }).remove().exec();
  });
}
module.exports.importUserList = (root)=>{
      var r = 'guest';

    role.findOne({name : r}).exec(function (err, docs) {
      for (var i in users_list) {
          let p = users_list[i]
          let newUser = new user({identifiant:p.identifiant , username:p.cin ,password :p.cin , cin :p.cin});
          let newProfile = new profile({
            name : p.name ,
            forname : p.forname ,
            tel :p.tel ,
            avatar :p.avatar ,
            function : p.function,
            region: p.region ,
            gouvernorat:p.gouvernorat ,
            avatar : `${p.identifiant}.jpg`,
            room : p.room
          });
          newUser.role = docs._id;
          newUser.save().then(res=>{
            newProfile.user = res._id ;
            newProfile.save().then(res=>{
              user.findOneAndUpdate(
              { _id: newProfile.user},
              { $set: {profile : newProfile._id} },
              { returnNewDocument: true }
            ).exec()
        });
          })


  }})

}

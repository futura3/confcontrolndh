import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLList,
  GraphQLID,
  GraphQLInputObjectType
 } from 'graphql'
import profileType from '../profile/profileType';
import roleType from '../role/roleType';
import workshopType from '../Workshops/workshopType';

export default new GraphQLObjectType({
  name: 'User',
  description: 'User object',
  fields: () => ({
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    id: {
      type: GraphQLString
    },
    username: {
      type: new GraphQLNonNull(GraphQLString)
    },
    cin: {
      type: GraphQLString
    },
    identifiant :{
      type : GraphQLString
    },
    password:{
      type: GraphQLString
    },
    role:{
      type: roleType
    },
    status: {
      type: GraphQLString
    },
    profile: {
        type: profileType
      },
    workshop: {
        type: workshopType
      }
  })
});

import React from 'react';
import Drawer from 'material-ui/Drawer';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import InboxIcon from 'material-ui-icons/Inbox';
import DraftsIcon from 'material-ui-icons/Drafts';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import ChevronRightIcon from 'material-ui-icons/ChevronRight';
import PersonAdd from 'material-ui-icons/PersonAdd';
import GroupWork from 'material-ui-icons/GroupWork';
import VerifiedUser from 'material-ui-icons/VerifiedUser';
import AssignmentInd from 'material-ui-icons/AssignmentInd'
import Lock from 'material-ui-icons/Lock';
import {Link} from 'react-router-dom';
import { withStyles } from 'material-ui/styles';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import Button from 'material-ui/Button';

import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { connect } from 'react-redux';
import { compose } from 'react-apollo';

import NavBarContainer from '../../containers/NavBarContainer';
import BottomToolbarContainer from '../../containers/BottomToolbarContainer';
import Directions from 'material-ui-icons/Directions';
import SupervisorAccount from 'material-ui-icons/SupervisorAccount'

const drawerWidth = 240 ;
const container = {
    overflow: 'auto',
    marginBottom: '56px',
};
const styles = theme => ({
  drawerPaper: {
    //height: '100%',
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    width: '100%',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    height: 'calc(100% - 56px)',
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      content: {
        height: 'calc(100% - 64px)',
        marginTop: 64,
      },
    },
  },
  'content-left': {
    marginLeft: -drawerWidth,
  },
  'content-right': {
    marginRight: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'contentShift-left': {
    marginLeft: 0,
  },
  'contentShift-right': {
    marginRight: 0,
  } ,
   appFrame: {
     position: 'relative',
     display: 'flex',
     width: '100%',
     height: '100%',
   },
   appBar: {
     position: 'absolute',
     transition: theme.transitions.create(['margin', 'width'], {
       easing: theme.transitions.easing.sharp,
       duration: theme.transitions.duration.leavingScreen,
     }),
   },
   appBarShift: {
     width: `calc(100% - ${drawerWidth}px)`,
     transition: theme.transitions.create(['margin', 'width'], {
       easing: theme.transitions.easing.easeOut,
       duration: theme.transitions.duration.enteringScreen,
     }),
   },

});
class AdminContainer extends React.Component{
  constructor(props){
    super(props);
    this.state ={
      drawer_open :false ,
      start_session :false ,
      end_session : false ,
    }
  }

  handleDialogOpen = () => {
    this.setState({ start_session: true });
  };
  handleSnackBarClosing =()=>{
    this.setState({ open_snackbar: false });
  }
  handleEndDialogOpen = () => {
      this.setState({ end_session: true });
    };
  handleSessionCreation =()=>{
    this.props.createSessionMutation()
      .then((response) => {
        this.setState({ start_session: false , open_snackbar:true });
        this.openSnackBarControl("Session created successfully" )
      })
    .catch((err) => {
      console.error(err);
    });
}
handleSessionCancellation =()=>{
  this.props.stopSessionMutation()
    .then((response) => {
      this.setState({ end_session: false , open_snackbar:true });
      this.openSnackBarControl("Session stopped successfully")

    })
    .catch((err) => {
      console.error(err);
    });
  }

  handleDialogClose = () => {
    this.setState({ start_session: false });
  };
  handleEndDialogClose = () => {
    this.setState({ end_session: false });
  };
  handleDrawerOpen = () =>{
       this.setState({ drawer_open: true });
  }
  handleDrawerClose=()=>{
    console.log('closed')
    this.setState({ drawer_open: false });

};

  render(){
    const {classes} = this.props
    console.log(this.props)
    const drawer = (
      <Drawer
         anchor="left"
         open={this.state.drawer_open}
         onClose={this.handleDrawerClose}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerInner}>
          <div className={classes.drawerHeader}>
            <IconButton onClick={this.handleDrawerClose}>
              <ChevronRightIcon />
            </IconButton>
          </div>
          <Divider />
          <Link to="/manageagents" style={{ textDecoration: 'none' }}>
            <ListItem button>
              <ListItemIcon>
                <VerifiedUser/>
              </ListItemIcon>
              <ListItemText primary="Manage Agents" />
            </ListItem>
          </Link>
          <Link to="/manageguest" style={{ textDecoration: 'none' }}>
            <ListItem button>
              <ListItemIcon>
               <AssignmentInd />
              </ListItemIcon>
              <ListItemText primary="Manage Guests" />
            </ListItem>
          </Link>
          <Link to="/adduser" style={{ textDecoration: 'none' }}>
            <ListItem button>
              <ListItemIcon>
                <PersonAdd />
              </ListItemIcon>
              <ListItemText primary="Add Attendee" />
            </ListItem>
          </Link>

            {this.props.data.activeSession==null &&(
              <ListItem button onClick={this.handleDialogOpen}>
                <ListItemIcon>
                  <GroupWork />
                </ListItemIcon>
              <ListItemText primary="Start Session"  />
          </ListItem>
            )}
              {this.props.data.activeSession!=null &&(<ListItem button onClick= {this.handleEndDialogOpen}>
          <ListItemIcon>
            <Lock />
          </ListItemIcon>
          <ListItemText primary="Close Session" />
        </ListItem>            )}
        </div>
      </Drawer>

    );
    return (
        <div  style={container}>
              <NavBarContainer
                handleDrawerOpen = {this.handleDrawerOpen}
              />
              <Dialog open={this.state.start_session} onRequestClose={this.handleDialogClose}>
              <DialogTitle>{"Confirm starting a new session"}</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  Are you sure that you want to start a new Session ...Creating a new session will automatically stop any other opened sessions
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleSessionCreation} color="primary">
                  Yes I''m sure
                </Button>
                <Button onClick={this.handleDialogClose} color="primary" autoFocus>
                  Discard
                </Button>
              </DialogActions>
            </Dialog>
            <Dialog open={this.state.end_session} onRequestClose={this.handleEndDialogClose}>
                  <DialogTitle>{"Confirm Ending the current session"}</DialogTitle>
                  <DialogContent>
                    <DialogContentText>
                      Are you sure that you want to stop the current Session ...This operation will automatically exit all attendies
                    </DialogContentText>
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={this.handleSessionCancellation} color="primary">
                      Yes I''m sure
                    </Button>
                    <Button onClick={this.handleEndDialogClose} color="primary" autoFocus>
                      Discard
                    </Button>
                  </DialogActions>
                </Dialog>
          {drawer}
          {this.props.children}

          <BottomToolbarContainer/>
        </div>
  );

  }
}
const createSessionMutation = gql`
  mutation createSession{
    createSession {
      _id
      start_hour
    }
  }
`;
const stopSessionMutation = gql`
  mutation stopSessionMutation{
    closeSession {
      _id
      start_hour
    }
  }
`;
const activeSession = gql`
  query activeSession{
    activeSession {
      _id
      start_hour
    }
  }
`;
///const contwithquery = graphql(getactiveSession)(AdminContainer);
const AdminContainerWithMutation = compose(
graphql(activeSession),
 graphql(createSessionMutation, {
   name : 'createSessionMutation'}),
 graphql(stopSessionMutation, {
   name: 'stopSessionMutation'
 })
)(AdminContainer)
export default withStyles(styles)(AdminContainerWithMutation);

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Switch from 'material-ui/Switch';
import  LoginBottomToolbar from '../Login/LoginBottomToolbar';
import NavBarContainer from '../../containers/NavBarContainer';
import BottomToolbarContainer from '../../containers/BottomToolbarContainer';
import Directions from 'material-ui-icons/Directions';
import AdminContainer from '../AdminSpace/AdminContainer';

const styles = theme => ({
  root: {
    marginTop: theme.spacing.unit * 3,
    width: '100%',
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  exitButton:{
      margin: 0,
      top: "auto",
      left: 20,
      bottom: 20,
      right: "auto",
      position: "fixed",
      background: 'linear-gradient(45deg, #16164C 30%, #803B5F 90%)',


  }


});

class AppShell extends React.Component {
    state={
      drawer_open : false ,

      open_snackbar: false,
      vertical: null,
      horizontal: null,
      message : "test message"
      }

      handleDrawerOpen = () =>{
        console.log(React.Children.toArray(this.props.children))
           this.setState({ drawer_open: true });
      }
      handleDrawerClose=()=>{
        this.setState({ drawer_open: false });
      }

    	render() {
        const { classes  } = this.props;
        const { vertical, horizontal } = this.state;


    		return (
    			<div>

            <NavBarContainer
              handleDrawerOpen = {this.handleDrawerOpen}
            />
    				<div id="content">
    					{this.props.children}
    				</div>

            <BottomToolbarContainer/>
    			</div>
    		);
    	}
}

AppShell.propTypes = {
	title: PropTypes.string,
	children: PropTypes.node
};



export default withStyles(styles)(AppShell);

import { observable, action, computed, useStrict } from 'mobx';
import axios from 'axios';
import { createApolloFetch } from 'apollo-fetch';
useStrict(true);


const fetch = createApolloFetch({
  uri: 'https://1jzxrj179.lp.gql.zone/graphql',
});

fetch({
  query: '{ posts { title }}',
}).then(res => {
  console.log(res.data);
});

// You can also easily pass variables for dynamic arguments
fetch({
  query: `query PostsForAuthor($id: Int!) {
    author(id: $id) {
      firstName
      posts {
        title
        votes
      }
    }
  }`,
  variables: { id: 1 },
}).then(res => {
  console.log(res.data);
});
class UserStore {
    // Values marked as 'observable' can be watched by 'observers'
    @observable users = [];
    @observable selectedUser = {};
    @computed get selectedId() { return this.selectedUser.id; }
    // In strict mode, only actions can modify mobx state
    @action setUsers = (users) => { this.users = [...users]; }
    @action selectUser = (user) => { this.selectedUser = user; }
    // Managing how we clear our observable state
    @action clearSelectedUser = () => { this.selectedUser = {}; }
    // An example that's a little more complex
    @action getUsers() {
	//Managing Async tasks like ajax calls with Mobx actions
	axios.get('http://jsonplaceholder.typicode.com/users').then( response => {
	    this.setUsers(response.data);
	});
    }
}

const store = new UserStore();

export default store;
export { UserStore };
